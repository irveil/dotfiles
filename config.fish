#-> config.fish
# Environment variables.
set PATH   /home/salieri/Tools /home/salieri/go/bin $PATH
set GOPATH /home/salieri/go


# Tool settings.
set FZF_DEFAULT_COMMAND 'rg --files --hidden --no-ignore --follow --glob "!.git/*"'
set FZF_CTRL_T_COMMAND $FZF_DEFAULT_COMMAND


# Aliases.
alias ls   'exa -x --group-directories-first'
alias ll   'exa -l --group-directories-first'
alias la   'exa -ax --group-directories-first'
alias lsg  'exa -l --group-directories-first --git'
alias tree 'exa --tree'

# Prompt.
function fish_prompt
	set_color 303033
	printf (pwd | sed s:$GOPATH:go: | sed s:$HOME:~:)
	set_color f20122
	printf ' ⬢ '  # ' ◆ '
	set_color normal
end


# Commands.
function watch
	while true
	clear
	exa --tree --group-directories-first
	sleep 5
	end
end

