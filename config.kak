#-> kakrc

# UI
addhl global number_lines -separator '  '
addhl global show_matching
set-option global ui_options ncurses_assistant=none
colorscheme gestalt


# Behaviour
set global indentwidth 4
set global tabstop 4

## Clipboard integration
map global normal p '!xsel --output --clipboard<ret>'
hook global NormalKey y|d|c %{ nop %sh{
	printf %s "$kak_reg_dquote" | xsel --input --clipboard
}}


# Keymap
map global normal <space> , -docstring 'leader'
map global normal <a-k> ':move-line-above %val{count}<ret>'
map global normal <a-j> ':move-line-below %val{count}<ret>'
map global user / ':comment-line<ret>' -docstring 'Comment'
map global user s :auto-pairs-surround<ret> -docstring 'Surround with'
map global user b :rofi-buffers<ret> -docstring 'Select buffer'
map global user f :rofi-files<ret> -docstring 'Open file'
map global user c :change-working-directory<space> -docstring 'Open folder'
map global user d :db<ret> -docstring 'Delete buffer'
map global user w :w<ret>  -docstring 'Write buffer'
map global user q :q<ret>  -docstring 'Quit'


# Language config
## Common
hook global WinCreate .* %{
  auto-pairs-enable
}

## Go
hook global WinSetOption filetype=go %{
	set window indentwidth 0
	set window tabstop 4
	set window formatcmd 'goimports'
	go-enable-autocomplete

    # Make iota, nil, true, false highlight as keywords.
	add-highlighter shared/go/code regex \b(nil|iota|true|false)\b 0:keyword
	# Highlight operators.
	add-highlighter shared/go/code regex =|:=|!=|&&|\|\||\*|-|\+|/|^|!|<|>|>=|<=|==|<- 0:operator
}


# Commands
define-command rofi-files \
-docstring 'Select files in project using Ripgrep and Rofi' %{ %sh{
    FILE=$(rg --files -j 4 --no-ignore --hidden --follow --glob '!.git/*' | \
    	   rofi -dmenu -theme gestalt -match fuzzy)
    if [ -n "$FILE" ]; then
        printf 'eval -client %%{%s} edit %%{%s}\n' "${kak_client}" "${FILE}" | kak -p "${kak_session}"
    fi
} }


define-command rofi-buffers \
-docstring 'Select an open buffer using Rofi' %{ %sh{
    BUFFER=$(printf %s\\n "${kak_buflist}" | tr : '\n' | rofi -dmenu -theme gestalt -match fuzzy)
    if [ -n "$BUFFER" ]; then
    	echo "eval -client '$kak_client' 'buffer ${BUFFER}'" | kak -p ${kak_session}
    fi
} }

