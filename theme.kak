#-> .config/kak/colors/gestalt.kak

%sh{
    fg='rgb:fafafa'
    bg='rgb:111210'
    accent='rgb:e19c64'
    dark='rgb:434752'
    darker='rgb:2b2e37'

    echo "
    # Code
    face global value ${dark}+b
    face global type ${accent}+b
    face global identifier ${fg}
    face global string ${dark}
    face global keyword ${accent}+b
    face global operator ${accent}+b
    face global comment ${darker}
    
    # UI
    face global Default ${fg},${bg}
    face global PrimarySelection ${fg},rgb:222225
    face global SecondarySelection ${fg},rgb:222225
    face global PrimaryCursor ${bg},${accent}+b
    face global SecondaryCursor ${bg},${accent}+b
    
    face global LineNumbers rgb:26282f
    face global MenuForeground rgb:f40122,rgb:141414+b
    face global MenuBackground default,rgb:141414
    
    face global StatusLine ${dark}
    face global StatusLineMode ${accent}+b
    face global StatusLineInfo ${dark}
    face global StatusLineValue ${dark}+b
    face global Prompt ${accent}+b
    "
}

