# Simple minimalistic configuration for void linux.
#
# Each file in dotfile directory beginning with 'config.' will be symlinked to
# .config directory, with the target name set by special '#-> TARGET' comment.
# Similarly 'local.' files will be symlinked to .local directory.
# NOTE: 'theme.' files need to specify whole target path, while other files
# 		only require final filename.
#
# You can customize your installation by adding additional dotfiles,
# packages and editing the 'all' target.


# Packages
packages = '                                     \
	xf86-video-intel xf86-input-libinput         \
	alsa-utils apulse                            \
	xorg-minimal bspwm sxhkd rofi                \
	kakoune alacritty exa fzf ripgrep fish-shell \
	unrar unzip xsel jq                          \
	font-hack-ttf font-sourcecodepro             \
	                                             \
	docker docker-compose                        \
'
nonfree-packages = '                             \
	opera                                        \
'
go-packages = '                                  \
	github.com/cjbassi/gotop                     \
	github.com/nats-io/go-nats                   \
	github.com/nats-io/gnatsd                    \
	github.com/nats-io/nats-top                  \
	golang.org/x/tools/cmd/goimports             \
	github.com/zmb3/gogetdoc                     \
	golang.org/x/tools/cmd/gorename              \
	github.com/nsf/gocode                        \
'
python-packages = '                              \
	ps_mem                                       \
'


# Perform full system installation.
all: install install-nonfree install-go install-python link-files
	@echo 'System is configured and ready to use.'


# Update system repositories and install updates.
update:
	sudo xbps-install -S
	sudo xbps-install -uy


# Install system packages.
install: update
	sudo xbps-install -y {{packages}}


# Install nonfree repository  and packages.
install-nonfree: update
	sudo xbps-install -y void-repo-nonfree
	sudo xbps-install -Sy
	sudo xbps-install -y {{nonfree-packages}}


# Install go compiler and go packages.
install-go: update
	sudo xbps-install -y go
	go get -u {{go-packages}}


# Install python3, pip and python packages.
install-python: update
	sudo xbps-install -y python3 python3-pip
	sudo pip install {{python-packages}}


# Symlink dotfiles.
link-files: #install
	#! /usr/bin/fish
	mkdir -p ~/.config
	echo 'Linking files:'
	for file in config.*
		echo $file
		set target_dir (echo $file | sed 's/config.//')
		set target     (head -n 2 $file | rg '(#|;+|//|/\*)-> ' | sed -r 's/(\/\*|#|;+|\/\/)-> //')
		mkdir -p ~/.config/$target_dir
		ln -si ~/dotfiles/$file ~/.config/$target_dir/$target 
	end
	
	for file in theme.* plugin.*
		echo $file
		set target     (head -n 2 $file | rg '(#|;+|//|/\*)-> ' | sed -r 's/(\/\*|#|;+|\/\/)-> //')
		set target_dir (echo $target | sed -r 's/[^/]+$//')
		mkdir -p ~/$target_dir
		ln -si ~/dotfiles/$file ~/$target 
	end
	
	for file in system.*
		set user_home ~
		echo $file
		set target     (head -n 2 $file | rg '(#|;+|//|/\*)-> ' | sed -r 's/(\/\*|#|;+|\/\/)-> //')
		set target_dir (echo $target | sed -r 's/[^/]+$//')
		sudo mkdir -p $target_dir
		sudo ln -si $user_home/dotfiles/$file $target 
	end

	# Link kakoune runtime directory.
	mkdir -p ~/.config/kak/autoload
	ln -s /usr/share/kak/autoload/* ~/.config/kak/autoload/

